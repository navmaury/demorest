package com.usbank.test;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usbank.pojo.Flags;
import com.usbank.pojo.Pojo;
import com.usbank.pojo.PojoSubmitJoke;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestApis {
	
	static String endpoint1="https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist";
	
	static String submitJokeEndpoint="https://sv443.net/jokeapi/v2/submit";
	
	
	@Test
	public void testGetResponse() {
		

        Pojo getResponse=given().contentType(ContentType.JSON).expect().defaultParser(Parser.JSON).
                when().get(String.format(endpoint1)).as(Pojo.class);

        System.out.println("Category is:="+getResponse.getCategory());
        System.out.println("Type is is:="+getResponse.getType());
        System.out.println("ID is is:="+getResponse.getId());
        
        Assert.assertEquals("Programming", getResponse.getCategory());
		
		
	}
	
	
	@Test
	public void testPutResponse() throws JsonProcessingException {
		RestAssured.baseURI=submitJokeEndpoint;
		RequestSpecification reqes=RestAssured.given();
		Flags flags=new Flags();
		flags.setNsfw(true);
		flags.setPolitical(false);
		flags.setRacist(true);
		flags.setReligious(false);
		flags.setSexist(true);
		
		PojoSubmitJoke joke=new PojoSubmitJoke();
		joke.setCategory("Programming");
		joke.setFormatVersion(2);
		joke.setType("single");
		joke.setJoke("Its good day");
		joke.setFlags(flags);
		
		ObjectMapper obj=new ObjectMapper()	;
		String jason=obj.writeValueAsString(joke);
		reqes.body(jason);
		Response r=reqes.request("PUT");		
		System.out.println("Resource is created on server and response is:-"+r.getStatusCode());
		Assert.assertEquals(201, r.getStatusCode());
	       
	 

	 
				
		
	}

}
